package com.pap.activitiy.dto.task;

import java.io.Serializable;

/**
 * 我的请求相关对象
 * @author alexgaoyh
 *
 */
public class HistoricProcessDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 已持续时间
	 */
	private Long durationInMillis;
	
	/**
	 * 业务单据号
	 */
	private String businessKey;

	/**
	 * 自定义的流程实例名称
	 * 在使用 runtimeService.setProcessInstanceName 更改实例自定义名称的时候，对应的自定义实例名称的值
	 */
	private String processCustomerDefinitionName;

	/**
	 * 流程实例名称
	 */
	private String processDefinitionName;
	
	/**
	 * 流程实例编码
	 */
	private String processInstanceId;
	
	/**
	 * 删除原因
	 */
	private String deleteReason;

	public Long getDurationInMillis() {
		return durationInMillis;
	}

	public void setDurationInMillis(Long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getProcessCustomerDefinitionName() {
		return processCustomerDefinitionName;
	}

	public void setProcessCustomerDefinitionName(String processCustomerDefinitionName) {
		this.processCustomerDefinitionName = processCustomerDefinitionName;
	}

	public String getProcessDefinitionName() {
		return processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	@Override
	public String toString() {
		return "HistoricProcessDTO{" +
				"durationInMillis=" + durationInMillis +
				", businessKey='" + businessKey + '\'' +
				", processCustomerDefinitionName='" + processCustomerDefinitionName + '\'' +
				", processDefinitionName='" + processDefinitionName + '\'' +
				", processInstanceId='" + processInstanceId + '\'' +
				", deleteReason='" + deleteReason + '\'' +
				'}';
	}

}
