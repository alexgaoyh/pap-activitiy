package com.pap.activitiy.dto.task;

import java.io.Serializable;

public class HistoricActivityInstanceDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 任务ID
	 */
	private String taskId;

	/**
	 * 流程实例ID
	 */	
	private String processInstanceId;

	/**
	 * 活动名称
	 */	
	private String activityName;

	/**
	 * 办理人
	 */	
	private String assignee;
	
	/**
	 * 办理人名称
	 */	
	private String assigneeName;

	/**
	 * 开始时间
	 */	
	private String startTime;

	/**
	 * 结束时间
	 */	
	private String endTime;
	
	/**
	 * 批注
	 */
	private String comment;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
