package com.pap.activitiy.dto.task;

import java.io.Serializable;

public class TaskDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 任务ID
	 */
	private String taskId;
	
	/**
	 * 任务名称
	 */	
	private String taskName;

	/**
	 * 实例名称
	 */
	private String processInstanceName;

	/**
	 * 实例对应的业务编号
	 */
	private String processBusinessKey;
	
	/**
	 * 任务的创建时间
	 */	
	private String taskCreateTime;
	
	/**
	 * 任务的办理人
	 */	
	private String taskAssignee;
	
	/**
	 * 流程实例ID
	 */	
	private String processInstanceId;
	
	/**
	 * 执行对象ID
	 */	
	private String executionId;
	
	/**
	 * 流程定义ID
	 */	
	private String processDefinitionId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getProcessInstanceName() {
		return processInstanceName;
	}

	public void setProcessInstanceName(String processInstanceName) {
		this.processInstanceName = processInstanceName;
	}

	public String getProcessBusinessKey() {
		return processBusinessKey;
	}

	public void setProcessBusinessKey(String processBusinessKey) {
		this.processBusinessKey = processBusinessKey;
	}

	public String getTaskCreateTime() {
		return taskCreateTime;
	}

	public void setTaskCreateTime(String taskCreateTime) {
		this.taskCreateTime = taskCreateTime;
	}

	public String getTaskAssignee() {
		return taskAssignee;
	}

	public void setTaskAssignee(String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
}
