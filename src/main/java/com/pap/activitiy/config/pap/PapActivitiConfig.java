package com.pap.activitiy.config.pap;

import com.pap.activitiy.config.pap.listener.PapGlobalProcessActivitiEventListener;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Activiti 配置类
 */
@Component
public class PapActivitiConfig implements ProcessEngineConfigurationConfigurer {

    @Autowired
    private PapGlobalProcessActivitiEventListener papGlobalProcessActivitiEventListener;

    @Override

    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {

        List<ActivitiEventListener> activitiEventListener=new ArrayList<ActivitiEventListener>();

        activitiEventListener.add(papGlobalProcessActivitiEventListener );//配置全局监听器

        processEngineConfiguration.setEventListeners(activitiEventListener);

    }

}