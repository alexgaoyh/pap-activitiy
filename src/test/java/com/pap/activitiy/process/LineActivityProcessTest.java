package com.pap.activitiy.process;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;

public class LineActivityProcessTest {

	static void log(Object obj) {
		System.out.println(obj);
	}

	static void run(ProcessEngine processEngine) throws Exception {
		RepositoryService repositoryService = processEngine.getRepositoryService();

		repositoryService.createDeployment().addClasspathResource("process/line.bpmn20.xml").deploy();

//		RuntimeService runtimeService = processEngine.getRuntimeService();
//
//		Map<String, Object> variables = new HashMap<>();
//		variables.put("userId", "14693732217267200");
//
//		runtimeService.startProcessInstanceByKey("line_process", variables);

	}

	public static void main(String[] args) {
		ProcessEngine processEngine = ProcessEngineConfiguration
				.createProcessEngineConfigurationFromResource("activiti.cfg.xml").buildProcessEngine();

		try {
			run(processEngine);
		} catch (Exception e) {
			e.printStackTrace();
		}

		processEngine.close();
	}
	
}
